1. Install MySQL (dev.mysql.com) on your machine. Choose the OS specific package.
2. While installing, it will ask for password for root - give a password <rootpasswd> and remember it.
3. Once installation is complete - put the mysql client in your path
4. Open mysql prompt using command
  $ mysql -u root
5. It will ask for password - enter it and you should be inside MySQL shell
6. First create a user
  mysql> CREATE USER 'i2tb'@'localhost' IDENTIFIED BY 'i2tb';
  mysql> GRANT ALL PRIVILIGES ON *.* to 'i2tb'@'localhost';
  mysql> FLUSH PRIVILEGES;
  mysql> quit
7. Now login as user 'i2tb'
  $ mysql -u i2tb
8. It will ask for password - enter i2tb
9. Once inside, create a new database
  mysql> CREATE DATABASE DGK;
  mysql> USE DGK;
  mysql> source create_tables.sql; 
  mysql> source upload_refdata.sql; 

above give full pathname for the file
then check if data is loaded by running SELECT command

  mysql> SELECT * FROM DK_STATE;

For running microservice
- make sure nodejs is installed and npm is installed
- go to server directory
  $ cd server
- install packages by running
  $ npm install
- it will install all the packages in package.json
- now go to src/server directory
  $ cd src/server
- start the server using the following command
  $ DEBIUG=* node server.js
- test the functions using curl
- go to another terminal and use the following curl command
  $ curl -i -XGET localhost:8887/states
  $ curl -i -XGET localhost:8887/states/KA
  $ curl -i -XGET localhost:8887/states/1
  $ curl -i -XGET localhost:8887/states/1/districts
