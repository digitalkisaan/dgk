var _ = require('lodash')
var async = require('async-q')
var Q = require('q')
var mysql = require('mysql')

var debug = require('debug')('dgk/server')
var error = debug
error.log = console.error.bind(console)

var restify = require('restify')
var corsMiddleware = require('restify-cors-middleware');

var cors = corsMiddleware({
       preflightMaxAge: 5,
       origins: ['*'],
       allowHeaders:['X-App-Version'],
       exposeHeaders:[]
     });
var conn = mysql.createConnection({ host: 'localhost', user: 'sanjay', password: 'sanjay', database: 'DIGITALKISSAN'})


var server = restify.createServer()

server.pre(restify.pre.userAgentConnection())
server.pre(cors.preflight);
server.use(cors.actual);
server.use(restify.plugins.queryParser())
server.use(restify.plugins.bodyParser())
server.use(restify.plugins.gzipResponse())
server.use(restify.plugins.acceptParser(server.acceptable))

var errors = require('restify-errors')

server.get('/states', function(req, res, next) {
  var fields = ['ID', 'STATE_NAME', 'STATE_CODE', 'STATE_INFO', 'CREATED_BY', 'CREATED_AT']
  var query  = 'SELECT ' + fields.join(',') + ' FROM DK_STATE'

  debug('query:', query)

  return Q.ninvoke(conn, 'query', query)
  .spread(function(results, fields) {
    var ret = []

    _.each(results, function(row) {
      ret.push(row)
    })
    res.json(ret)
    next()
  })
  .catch(function(err) {
    error('Error in getting states:', err)
    res.send(400, err.toString())
  })
})

server.get('/states/:state_id', function(req, res, next) {
  debug('state_id:', req.params.state_id)
  var whereClause = null

  if (req.params.state_id.length == 2 && !/\d{2}/.test(req.params.id)) { // it is state_code
    whereClause = 'WHERE STATE_CODE=\'' + req.params.state_id + '\''
  }
  else { // it is id
    whereClause = 'WHERE ID=' + req.params.state_id.toString()
  }

  var fields = ['ID', 'STATE_NAME', 'STATE_CODE', 'STATE_INFO', 'CREATED_BY', 'CREATED_AT']
  var query  = 'SELECT ' + fields.join(',') + ' FROM DK_STATE ' + whereClause

  debug('query:', query)

  return Q.ninvoke(conn, 'query', query)
  .spread(function(results, fields) {
    var ret = results[0]

    res.json(ret)
    next()
  })
  .catch(function(err) {
    error('Error in getting states:', err)
    res.send(400, err.toString())
  })
})

server.get('/districts/:district_id', function(req, res, next) {
  debug('district_id:', req.params.district_id)
  var whereClause = 'WHERE ID=' + req.params.district_id.toString()

  var fields = ['ID', 'DISTRICT_NAME', 'DISTRICT_CODE', 'DISTRICT_INFO', 'CREATED_BY', 'CREATED_AT']
  var query  = 'SELECT ' + fields.join(',') + ' FROM DK_DISTRICT ' + whereClause

  debug('query:', query)

  return Q.ninvoke(conn, 'query', query)
  .spread(function(results, fields) {
    var ret = results[0]

    res.json(ret)
    next()
  })
  .catch(function(err) {
    error('Error in getting districts:', err)
    res.send(400, err.toString())
  })
})

server.get('/tehsils/:tehsil_id', function(req, res, next) {
  debug('tehsil_id:', req.params.tehsil_id)
  var whereClause = 'WHERE ID=' + req.params.tehsil_id.toString()

  var fields = ['ID', 'TEHSIL_NAME', 'TEHSIL_CODE', 'TEHSIL_INFO', 'CREATED_BY', 'CREATED_AT']
  var query  = 'SELECT ' + fields.join(',') + ' FROM DK_TEHSIL ' + whereClause

  debug('query:', query)

  return Q.ninvoke(conn, 'query', query)
  .spread(function(results, fields) {
    var ret = results[0]

    res.json(ret)
    next()
  })
  .catch(function(err) {
    error('Error in getting tehsils:', err)
    res.send(400, err.toString())
  })
})

server.get('/towns/:town_id', function(req, res, next) {
  debug('town_id:', req.params.town_id)
  var whereClause = 'WHERE ID=' + req.params.town_id.toString()

  var fields = ['ID', 'TOWN_NAME', 'TOWN_CODE', 'TOWN_INFO', 'CREATED_BY', 'CREATED_AT']
  var query  = 'SELECT ' + fields.join(',') + ' FROM DK_TOWN ' + whereClause

  debug('query:', query)

  return Q.ninvoke(conn, 'query', query)
  .spread(function(results, fields) {
    var ret = results[0]

    res.json(ret)
    next()
  })
  .catch(function(err) {
    error('Error in getting towns:', err)
    res.send(400, err.toString())
  })
})

server.get('/states/:state_id/districts', function(req, res, next) {
  debug('state_id:', req.params.state_id)
  var whereClause = 'WHERE STATE_ID=' + req.params.state_id.toString()

  var fields = ['ID', 'DISTRICT_NAME', 'DISTRICT_CODE', 'DISTRICT_INFO', 'CREATED_BY', 'CREATED_AT']
  var query  = 'SELECT ' + fields.join(',') + ' FROM DK_DISTRICT ' + whereClause

  debug('query:', query)

  return Q.ninvoke(conn, 'query', query)
  .spread(function(results, fields) {
    var ret = results[0]

    res.json(ret)
    next()
  })
  .catch(function(err) {
    error('Error in getting districts:', err)
    res.send(400, err.toString())
  })
})

server.get('/districts/:district_id/tehsils', function(req, res, next) {
  debug('district_id:', req.params.district_id)
  var whereClause = 'WHERE DISTRICT_ID=' + req.params.district_id.toString()

  var fields = ['ID', 'TEHSIL_NAME', 'TEHSIL_CODE', 'TEHSIL_INFO', 'CREATED_BY', 'CREATED_AT']
  var query  = 'SELECT ' + fields.join(',') + ' FROM DK_TEHSIL ' + whereClause

  debug('query:', query)

  return Q.ninvoke(conn, 'query', query)
  .spread(function(results, fields) {
    var ret = results[0]

    res.json(ret)
    next()
  })
  .catch(function(err) {
    error('Error in getting tehsils:', err)
    res.send(400, err.toString())
  })
})

server.get('/districts/:district_id/towns', function(req, res, next) {
  debug('district_id:', req.params.district_id)
  var whereClause = 'WHERE DISTRICT_ID=' + req.params.district_id.toString()

  var fields = ['ID', 'TOWN_NAME', 'TOWN_CODE', 'TOWN_INFO', 'CREATED_BY', 'CREATED_AT']
  var query  = 'SELECT ' + fields.join(',') + ' FROM DK_TOWN ' + whereClause

  debug('query:', query)

  return Q.ninvoke(conn, 'query', query)
  .spread(function(results, fields) {
    var ret = results[0]

    res.json(ret)
    next()
  })
  .catch(function(err) {
    error('Error in getting towns:', err)
    res.send(400, err.toString())
  })
})

server.get('/tehsils/:tehsil_id/towns', function(req, res, next) {
  debug('tehsil_id:', req.params.tehsil_id)
  var whereClause = 'WHERE TEHSIL_ID=' + req.params.tehsil_id.toString()

  var fields = ['ID', 'TOWN_NAME', 'TOWN_CODE', 'TOWN_INFO', 'CREATED_BY', 'CREATED_AT']
  var query  = 'SELECT ' + fields.join(',') + ' FROM DK_TOWN ' + whereClause

  debug('query:', query)

  return Q.ninvoke(conn, 'query', query)
  .spread(function(results, fields) {
    var ret = results[0]

    res.json(ret)
    next()
  })
  .catch(function(err) {
    error('Error in getting towns:', err)
    res.send(400, err.toString())
  })
})

server.listen(process.env.PORT || 8887)
debug('Server listening on', server.address())

